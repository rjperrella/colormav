#include <windows.h>
#include <stdio.h>
#include <conio.h>

/*
 * Description:
 * Simple program for filtering Apache Maven output using the Windows Console API.  It does not require ANSI.SYS or ANSICON to be loaded.
 * @author Ron Perrella
 * @copyright (C) 2012 Ronald Perrella
 *
 * License: BSD
 */


/*
 * Inefficient two pass comparison.
 *
 * @return 0 if dest does not start with src string.
 */

int startswith(char *dest, char *src)
{
	int j;

	j = strlen(src);
	return (strncmp(dest, src, j) == 0);
}

int main(int argc, char **argv)
{
	char line[100000]; // memory is cheap. Builds *usually* do not exceed. Truncation will occur.
	HANDLE hStdout;
	WORD attr, original_attr;
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if( (hStdout = GetStdHandle(STD_OUTPUT_HANDLE)) == INVALID_HANDLE_VALUE)
	{
		printf("Error: SetConsoleTextAttribute (%d).\n", GetLastError());
		exit(1);
	}
    // See if redirection is in effect.
    DWORD filetype = GetFileType(hStdout);
    int use_stdc = ((filetype == FILE_TYPE_PIPE)||(filetype == FILE_TYPE_DISK));

    // On redirect, don't colorize.
    if (use_stdc)
    {
        while(fgets(line, sizeof(line)-2, stdin))
        {
            printf("%s", line);
        }
    }
    else
    {
        if( !(GetConsoleScreenBufferInfo(hStdout, &csbi)))
        {
            printf("Error: GetConsoleScreenBufferInfo (%d).\n", GetLastError());
            exit(4);
        }

        attr = original_attr = csbi.wAttributes;


        while(fgets(line, sizeof(line)-2, stdin))
        {
            if (startswith(line, "[ERROR]") )
            {
                attr = FOREGROUND_RED|FOREGROUND_INTENSITY;
            }else
            if (startswith(line, "[debug]") )
            {
                attr = FOREGROUND_RED|FOREGROUND_BLUE;
            }else
            if (startswith(line, "[WARNING]") )
            {
                attr = FOREGROUND_RED|FOREGROUND_BLUE;
            }else
            if (startswith(line, "[INFO] BUILD FAILURE") )
            {
                attr = FOREGROUND_RED|FOREGROUND_INTENSITY;
            }else
            if (startswith(line, "[INFO] BUILD SUCCESS") )
            {
                attr = FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY;
            }else
            if (startswith(line, "[INFO]") )
            {
                attr = FOREGROUND_BLUE|FOREGROUND_GREEN;
            }else {
                attr = original_attr;
            }


            if( !SetConsoleTextAttribute(hStdout, attr))
            {
                printf("Error: SetConsoleTextAttribute (%d).\n", GetLastError());
                exit(2);
            }

            DWORD numWritten;
            WriteConsole(hStdout, line, strlen(line), &numWritten, NULL);
        }

        if( !SetConsoleTextAttribute(hStdout, original_attr))
        {
            printf("Error: last SetConsoleTextAttribute (%d).\n", GetLastError());
            exit(3);
        }
    }
	return 0;
}
