# README #

ColorMAV is a colorizer for Maven output for computers running Windows. It's written in plain C and is compiled with the free and excellent Digital Mars compiler.  The reason it is Windows-only is that it uses the Windows screen IO APIs.  Just pipe your maven output directly into colormav and you'll have colorized output.

### What is this repository for? ###

* Quick summary


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
There is nothing to setup. It is a single executable you can install anywhere.

* Configuration
There are no configuration files. Don't like a configuration? Just change the code! (I know, this isn't ideal but the program is pretty simple.)
* Dependencies
None.
* Database configuration
N/A
* How to run tests
No tests.
* Deployment instructions
Copy to your favorite tools directory and use.  You should probably make sure that directory is on the PATH.

### Contribution guidelines ###

* Writing tests
No need.

* Code review
Keep the same basic coding style.

* Other guidelines
None but I'm open to suggestion.

### Who do I talk to? ###

* Repo owner or admin
Me!
* Other community or team contact
